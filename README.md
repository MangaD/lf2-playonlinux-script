LF2 - PlayOnLinux Script
===

Script for installing [Little Fighter 2](http://lf2.net) on a **Linux** or **Mac** machine with [PlayOnLinux](https://www.playonlinux.com/) or [PlayOnMac](https://www.playonmac.com/) respectively.

Little Fighter 2 is a __Windows only__ game.

For running LF2 on Linux or Mac, WINE with additional libraries (vcrun2005, wmp9, quartz and devenum) is required. For LF2 Lobby vb6run is required.

Further instructions: [http://www.lf-empire.de/forum/showthread.php?tid=10042](http://www.lf-empire.de/forum/showthread.php?tid=10042)

PlayOnLinux script page: [https://www.playonlinux.com/en/app-2623-Little_Fighter_2_v20a.html](https://www.playonlinux.com/en/app-2623-Little_Fighter_2_v20a.html)